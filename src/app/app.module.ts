import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms'; // Para manejar formularios
import { HttpClientModule } from '@angular/common/http'; // Para poder usarlo en sevice

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { ConsoleComponent } from './components/console/console.component';
import { LoginComponent } from './components/login/login.component';
import { GraficoComponent } from './components/grafico/grafico.component';

@NgModule({
  declarations: [
    AppComponent,
    FormularioComponent,
    ConsoleComponent,
    LoginComponent,
    GraficoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,                       // Agrego el modulo aca, para q funcione
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
