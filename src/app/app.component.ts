import { Component, OnInit } from '@angular/core';
import { WebsocketService } from 'src/app/services/websocket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [WebsocketService]
})
export class AppComponent implements OnInit {
  title = 'userInterface';
  public WSS: WebsocketService = new WebsocketService();
  public logueado: Boolean = false;
  public clientID: String;
  public token: String;
  public estado: String = 'comando';
  public graficoID: number;

  constructor() {
    if (window.sessionStorage['session'] && window.sessionStorage['clientID'] && window.sessionStorage['token']) {
      this.logueado = window.sessionStorage['session'] as Boolean;
      this.clientID = window.sessionStorage['clientID'] as String;
      this.token = window.sessionStorage['token'] as String;
    }
  }

  ngOnInit() {
    this.WSS.newNotification();
    this.WSS.listenMsg();
  }

  loguear(valor) {
    this.estado = 'comando';
    this.logueado = valor.status;
    this.clientID = valor.clientID;
    this.token = valor.token;
    window.sessionStorage['clientID'] = this.clientID;
    window.sessionStorage['session'] = this.logueado;
    window.sessionStorage['token'] = this.token;
  }

  logout() {
    this.logueado = false;
    delete window.sessionStorage['session'];
    delete window.sessionStorage['clientID'];
    delete window.sessionStorage['token'];
    delete this.WSS.messageArray;
    this.WSS.messageArray = [];
  }

  setEstado(estado: String) {
    this.estado = estado;
  }

  setGrafico(id: number) {
    this.graficoID = id;
  }
}
