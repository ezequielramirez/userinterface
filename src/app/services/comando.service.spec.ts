import { TestBed } from '@angular/core/testing';

import { ComandoService } from './comando.service';

describe('ComandoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComandoService = TestBed.get(ComandoService);
    expect(service).toBeTruthy();
  });
});
