import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Network } from '../models/network';

@Injectable({
  providedIn: 'root'
})
export class NetworksService {

  networks: Network[];
  readonly URL_API = 'http://localhost:3001/networks';

  constructor(private http: HttpClient) { }

  getNetworks(token: String) {
    return this.http.post(this.URL_API, {token});
  }
}
