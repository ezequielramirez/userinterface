import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from '../models/login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  selectedLogin: Login;

  readonly url_login = 'http://localhost:3001/login';
  constructor(private http: HttpClient) {
    this.selectedLogin = new Login();
  }

  logIn(login: Login) {
    return this.http.post(this.url_login, login);
  }
}
