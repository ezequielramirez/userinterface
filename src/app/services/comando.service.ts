import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comando } from '../models/comando';

@Injectable({
  providedIn: 'root'
})
export class ComandoService {

  selectedCommand: Comando;
  readonly url_send = 'http://localhost:3001/send';
  readonly url_listen = 'http://localhost:3001/listen';

  constructor(private http: HttpClient) {
    this.selectedCommand = new Comando();
  }

  sendCommand(comando: Comando) {
    const msg = this.mapper(comando);
    return this.http.post(this.url_send, msg);
  }

  listen(comando: Comando) {
    const msg = this.mapper(comando);
    return this.http.post(this.url_listen, msg);
  }

  mapper(comando: Comando) {
    const msg = {
      clientID: comando.clientID,
      mac: comando.mac,
      typeOp: comando.typeOp,
      cid: comando.cid,
      param: comando.param,
      token: comando.token,
      message: undefined,
      config: undefined
    };
    if (comando.retries || comando.timeout || comando.network) {
      msg.config = {};
    }
    if (comando.network) {
      msg.config.network = comando.network;
    }
    if (comando.retries) {
      msg.config.retries = comando.retries;
    }
    if (comando.timeout) {
      msg.config.timeout = comando.timeout;
    }
    if (comando.message && (!comando.cid || comando.cid < 0)) {
      msg.message = comando.message;
    }
    return msg;
  }
}
