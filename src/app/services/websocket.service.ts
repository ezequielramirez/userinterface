import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  private url = 'http://localhost:3001';
  private socket = io(this.url);

  public messageArray: Array<{msg: String, status: boolean, respuesta: boolean }> = [];
  constructor() { }

  newNotification() {
    // tslint:disable-next-line:no-shadowed-variable
    const observable = new Observable<{msg: String, status: boolean}>(observable => {
      this.socket.on('notify', (data) => {
        observable.next(data);
      });
      return () => { this.socket.disconnect(); };
    });
    observable.subscribe(data => {
      this.messageArray.push({ msg: data.msg, status: data.status, respuesta: true });
      console.log(this.messageArray.length);
    });
  }

  listenMsg() {
    // tslint:disable-next-line:no-shadowed-variable
    const observable = new Observable<{msg: String, status: boolean}>(observable => {
      this.socket.on('listen', (data) => {
        observable.next(data);
      });
      return () => { this.socket.disconnect(); };
    });
    observable.subscribe(data => {
      this.messageArray.push({ msg: data.msg, status: data.status, respuesta: false });
    });
  }

  loggin(clientID: String) {
    this.socket.emit('loggin', clientID);
  }
}
