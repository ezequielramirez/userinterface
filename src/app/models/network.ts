export class Network {

  constructor(_id = '', nombre= '', networkID= '', connType= '', ip= '', puertoUDP= 0, puertoRF= 0) {
    this._id = _id;
    this.nombre = nombre;
    this.networkID = networkID;
    this.connType = connType;
    this.ip = ip;
    this.puertoUDP = puertoUDP;
    this.puertoRF = puertoRF;
  }

  _id: string;
  nombre: string;
  networkID: string;
  connType: string;
  ip: string;
  puertoUDP: number;
  puertoRF: number;
}
