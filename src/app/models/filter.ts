export class Filter {

  constructor() {
    const hoy = new Date();
    const dd = hoy.getDate();
    let diaD = (dd - 1).toString();
    let diaH = dd.toString();
    const mm = hoy.getMonth() + 1 ;
    let mes = mm.toString();
    const yyyy = hoy.getFullYear();

    if (dd - 1 < 10) {
      diaD = '0' + (dd - 1);
    }
    if (dd < 10) {
      diaH = '0' + dd;
    }
    if (mm < 10) {
      mes = '0' + mm;
    }
    this.desde = yyyy + '-' + mes + '-' + diaD;
    this.hasta = yyyy + '-' + mes + '-' + diaH;
    this.horaD = '00:00';
    this.horaH = '23:59';
    this.play = 't';
    this.intervalo = '60000';
  }
  desde: string;
  hasta: string;
  horaD: string;
  horaH: string;
  intervalo: string;
  play: string;
}
