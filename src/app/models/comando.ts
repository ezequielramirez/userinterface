export class Comando {
  constructor( clientID= '', target= '', typeOp= '') {

  }

  clientID: string;
  mac: string;
  typeOp: string;
  cid: number;
  param: string;
  message: string;
  retries: number;
  timeout: number;
  network: string;
  config: {};
  token: string;
}
