import { Component, OnInit, Input } from '@angular/core';
import { ComandoService } from '../../services/comando.service';
import { NetworksService } from '../../services/networks.service';
import { NgForm } from '@angular/forms';
import { Comando } from 'src/app/models/comando';
import { Network } from 'src/app/models/network';
import { WebsocketService } from 'src/app/services/websocket.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css'],
  providers: [WebsocketService]
})
export class FormularioComponent implements OnInit {

  @Input() WSS: WebsocketService;
  @Input() clientID: String;
  @Input() token: String;
  comando: Comando = new Comando;

  constructor(private comandoService: ComandoService, private networksService: NetworksService) {}

  ngOnInit() {
    this.getNetworks();
  }

  exeOp(form: NgForm) {
    this.WSS.loggin(this.clientID);  // Para registrar el socket del lado del servidor
    form.value.name = this.clientID;
    form.value.token = this.token;
    if (form.value.typeOp === 'send' || form.value.typeOp === 'cancelSend') {
      this.sendCommand(form);
    } else if (form.value.typeOp === 'listen' || form.value.typeOp === 'cancelListen') {
      this.listen(form);
    }
  }

  sendCommand(form: NgForm) {
    this.comandoService.sendCommand(form.value)
    .subscribe( res => {
      this.resetForm(form);
    });
  }

  listen(form: NgForm) {
    this.comandoService.listen(form.value)
    .subscribe( res => {
      this.resetForm(form);
    });
  }

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.getNetworks();
    }
  }

  getNetworks() {
    this.networksService.getNetworks(this.token)
      .subscribe( res => {
        this.networksService.networks = res as Network[];
      });
  }
}
