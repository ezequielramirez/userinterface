import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NgForm } from '@angular/forms';
import { Filter } from 'src/app/models/filter';

@Component({
  selector: 'app-grafico',
  templateUrl: './grafico.component.html',
  styleUrls: ['./grafico.component.css']
})
export class GraficoComponent implements OnInit, OnChanges {

  @Input() graficoID: number;
  filtro: Filter = new Filter();
  graficos: Array<string> = [
    // tslint:disable-next-line:max-line-length
    'http://198.58.96.88:5601/app/kibana#/dashboard/077a2430-9a4b-11e8-b9e7-4d4a6f6b9b4f?embed=true&_g=(refreshInterval:(pause:!{4},value:{5}),time:(from:\'{0}T{2}:59.000Z\',mode:absolute,to:\'{1}T{3}:00.000Z\'))&_a=(description:\'\',filters:!(),fullScreenMode:!f,options:(darkTheme:!t,hidePanelTitles:!f,useMargins:!f),panels:!((embeddableConfig:(spy:!n,vis:(legendOpen:!f)),gridData:(h:13,i:\'1\',w:24,x:24,y:0),id:\'52434d80-9a4a-11e8-b9e7-4d4a6f6b9b4f\',panelIndex:\'1\',type:visualization,version:\'6.3.0\'),(embeddableConfig:(),gridData:(h:13,i:\'3\',w:24,x:0,y:0),id:a9e21da0-7fb0-11e8-9456-71796c56575a,panelIndex:\'3\',type:visualization,version:\'6.3.0\'),(embeddableConfig:(),gridData:(h:16,i:\'4\',w:48,x:0,y:13),id:\'0afb92a0-9b23-11e8-ad33-890b552cbdf7\',panelIndex:\'4\',type:visualization,version:\'6.3.0\')),query:(language:kuery,query:\'\'),timeRestore:!t,title:Nucleadores,viewMode:view)',
    // tslint:disable-next-line:max-line-length
    'http://198.58.96.88:5601/app/kibana#/dashboard/00ca3c90-b848-11e8-a52f-070d2a1b9069?embed=true&_g=(refreshInterval:(pause:!{4},value:{5}),time:(from:\'{0}T{2}:59.000Z\',mode:absolute,to:\'{1}T{3}:59.000Z\'))&_a=(description:\'\',filters:!(),fullScreenMode:!f,options:(darkTheme:!t,hidePanelTitles:!f,useMargins:!f),panels:!((embeddableConfig:(),gridData:(h:15,i:\'1\',w:48,x:0,y:43),id:\'26d98ae0-b847-11e8-a52f-070d2a1b9069\',panelIndex:\'1\',type:visualization,version:\'6.4.0\'),(embeddableConfig:(),gridData:(h:12,i:\'2\',w:48,x:0,y:0),id:a9e21da0-7fb0-11e8-9456-71796c56575a,panelIndex:\'2\',type:visualization,version:\'6.4.0\'),(embeddableConfig:(),gridData:(h:13,i:\'4\',w:48,x:0,y:58),id:\'798286b0-b84d-11e8-a52f-070d2a1b9069\',panelIndex:\'4\',type:visualization,version:\'6.4.0\'),(embeddableConfig:(),gridData:(h:15,i:\'6\',w:48,x:0,y:28),id:ae216f00-ba8a-11e8-a52f-070d2a1b9069,panelIndex:\'6\',type:visualization,version:\'6.4.0\'),(embeddableConfig:(),gridData:(h:15,i:\'7\',w:48,x:0,y:71),id:\'154e05b0-ba8d-11e8-a52f-070d2a1b9069\',panelIndex:\'7\',type:visualization,version:\'6.4.0\'),(embeddableConfig:(),gridData:(h:14,i:\'8\',w:48,x:0,y:86),id:\'333af0e0-ba9e-11e8-a52f-070d2a1b9069\',panelIndex:\'8\',type:visualization,version:\'6.4.0\'),(embeddableConfig:(),gridData:(h:16,i:\'9\',w:48,x:0,y:12),id:\'041136c0-baa4-11e8-a52f-070d2a1b9069\',panelIndex:\'9\',type:visualization,version:\'6.4.0\')),query:(language:kuery,query:\'\'),timeRestore:!t,title:\'Test%20y%20CV\',viewMode:view)'
  ];
  safeSrc: SafeResourceUrl;
  url: string;

  constructor(private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.cargarIframe();
    this.cargarIframe();
  }

  filtrar(form: NgForm) {
    this.cargarIframe();
    this.cargarIframe();
  }

  cargarIframe() {
    const urlBase = this.graficos[this.graficoID];
    const url = this.StringFormat(urlBase, this.filtro);
    this.url = url;
    this.safeSrc =  this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  play() {
    if (this.filtro.play === 'f') {
      this.filtro.play = 't';
    } else {
      this.filtro.play = 'f';
    }
    this.cargarIframe();
  }

  StringFormat(url: string, filtro: Filter) {
    url = url.replace('{0}', filtro.desde);
    url = url.replace('{1}', filtro.hasta);
    url = url.replace('{2}', filtro.horaD);
    url = url.replace('{3}', filtro.horaH);
    url = url.replace('{4}', filtro.play);
    url = url.replace('{5}', filtro.intervalo);
    return url;
  }
}

