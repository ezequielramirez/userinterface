import { Component, OnInit, Input } from '@angular/core';
import { Comando } from 'src/app/models/comando';
import { WebsocketService } from 'src/app/services/websocket.service';

@Component({
  selector: 'app-console',
  templateUrl: './console.component.html',
  styleUrls: ['./console.component.css'],
  providers: [WebsocketService]
})
export class ConsoleComponent implements OnInit {

  @Input() WSS: WebsocketService;

  listenAll: boolean;

  constructor() {
  }

  ngOnInit() {
  }

  cleanConsole() {
    this.WSS.messageArray = [];
  }

}
