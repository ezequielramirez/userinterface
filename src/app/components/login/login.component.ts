import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { NgForm } from '@angular/forms';
import { Login } from 'src/app/models/login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output() loguear = new EventEmitter();
  private status: Boolean = false;
  private error: String = '';

  constructor(private loginService: LoginService) {
    this.status = false;
    this.error = '';
   }

  ngOnInit() {
  }

  login(form: NgForm) {
    this.loginService.logIn(form.value)
    .subscribe((data: {status: Boolean, error: String, token: String }) => {
      this.status = data.status;
      this.error = data.error;
      const salida = { status: this.status, clientID: form.value.name, token: data.token };
      this.loguear.emit(salida);
    });
  }
}
